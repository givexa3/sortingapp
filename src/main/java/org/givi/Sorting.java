package org.givi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Sorting {
    public String sort(int... values){
        if(values==null || values.length==0){
            System.out.println("[]");
            return "[]";
        }else if(values.length>0 && values.length<=10){
            Arrays.sort(values);
            System.out.println(Arrays.toString(values));
            return Arrays.toString(values);
        }else{
            throw new IndexOutOfBoundsException();
        }

    }

}
