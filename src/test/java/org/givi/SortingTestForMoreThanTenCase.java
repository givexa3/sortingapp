package org.givi;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class SortingTestForMoreThanTenCase {

    protected Sorting mySorting = new Sorting();

    private Integer[] array;

    public SortingTestForMoreThanTenCase(Integer[] arr){
        this.array = arr;
    }

    @Parameterized.Parameters
    public static Collection testCases() {
        return Arrays.asList(new Integer[][][] {
                {{1,2,3,4,5,6,7,8,9,10,11}},
                {{100,33,2,1,-1,0,0,5,5,5,6,7,8,9}},
                {{9,8,7,6,5,4,3,2,1,2,2,3,123,33,3}},
                {{0,0,0,0,0,-1,1,4,4,4,4,4,4,4,4,4}},
                {{1,1,1,1,1,1,1,1,2,3,4,5,6,7,8,9,10}},
                {{1,2,3,5,3,3,3,3,3,3,3,3,3,3,3,3,3}},
                {{1,5,9,8,7,6,5,4,3,-1,-100,-111,-1111,-22}}
        });
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void testForMoreThanTenCase()
    {
        int[] temp = new int[array.length];
        for(int i=0; i<array.length; i++){
            temp[i] = array[i].intValue();
        }
        mySorting.sort(temp);
    }

}