package org.givi;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class SortingTestForSingleCase {

    protected Sorting mySorting = new Sorting();

    private Integer[] array;

    public SortingTestForSingleCase(Integer[] arr){
        this.array = arr;
    }

    @Parameterized.Parameters
    public static Collection testCases() {
        return Arrays.asList(new Integer[][][] {
                {{1}},
                {{100}},
                {{-33}},
                {{0}},
                {{777}}
        });
    }


    @Test
    public void testForSingleCase()
    {
        int i = array[0].intValue();

        assertEquals("["+i+"]", mySorting.sort(i));
    }

}