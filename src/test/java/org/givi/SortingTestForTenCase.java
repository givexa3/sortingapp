package org.givi;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class SortingTestForTenCase {

    protected Sorting mySorting = new Sorting();

    private Integer[] array;

    public SortingTestForTenCase(Integer[] arr){
        this.array = arr;
    }

    @Parameterized.Parameters
    public static Collection testCases() {
        return Arrays.asList(new Integer[][][] {
                {{1,2,3,4,5,6,7,8,9,10}},
                {{100,33,2,1,-1,0,0}},
                {{9,8,7,6,5,4,3,2,1}},
                {{0,0,0,0,0,-1,1}},
                {{1,1,1,1,1,1,1}},
                {{1,2,3}},
                {{1,5}}
        });
    }

    @Test
    public void testForTenCase()
    {
        //checking if array is sorted correctly
        //if someone accidently, removed logic, from
        //Sorting.java the test would fail.
        int[] temp = new int[array.length];
        int[] temp2 = new int[array.length];

        for(int i=0; i<array.length; i++){
            temp[i] = array[i].intValue();
            temp2[i] = array[i].intValue();
        }
        Arrays.sort(temp2);
        assertEquals(Arrays.toString(temp2), mySorting.sort(temp));
    }

}