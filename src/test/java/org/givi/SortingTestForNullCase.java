package org.givi;

import org.junit.Test;

import java.util.Arrays;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class SortingTestForNullCase {

    protected Sorting mySorting = new Sorting();

    private Integer[] array;

    public SortingTestForNullCase(Integer[] arr){
        this.array = arr;

    }

    @Parameterized.Parameters
    public static Collection testCases() {
        return Arrays.asList(new Integer[][][] {
                {{}},
                {{}},
                {{null}},
                {{}},
                {{null}}
        });
    }

    @Test
    public void testForNullCase()
    {
        String excpeted = "";
        if(array.length==0){
            excpeted="[]";
        }else if(array[0]==null){
            excpeted="[]";
        }
        assertEquals("[]", excpeted);
    }

}